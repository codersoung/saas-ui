import request from '@/utils/request'

export function loadDataUrl() {
  return 'admin-service/api/roleMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/roleMng/doDelete'
}

export function exportUrl() {
  return 'admin-service/api/roleMng/doExport'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/roleMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/roleMng/toCopy',
    method: 'post',
    data
  })
}

export function findRoleByCpyCodeUrl() {
  return 'admin-service/api/roleMng/findRoleByCpyCode'
}

export function saveRoleForm(data) {
  return request({
    url: 'admin-service/api/roleMng/saveRoleForm',
    method: 'post',
    data
  })
}

export function findFormByRoleIdUrl() {
  return 'admin-service/api/roleMng/findFormByRoleId'
}

export function deleteRoleForm(data) {
  return request({
    url: 'admin-service/api/roleMng/deleteRoleForm',
    method: 'post',
    data
  })
}

export function saveUser(data) {
  return request({
    url: 'admin-service/api/roleMng/saveUser',
    method: 'post',
    data
  })
}

export function findUserByRoleIdUrl() {
  return 'admin-service/api/roleMng/findUserByRoleId'
}

export function deleteRoleUser(data) {
  return request({
    url: 'admin-service/api/roleMng/deleteRoleUser',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/roleMng/toView',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/roleMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/roleMng/doDelete',
    method: 'post',
    data
  })
}

export function savePriv(data) {
  return request({
    url: 'admin-service/api/roleMng/savePriv',
    method: 'post',
    data
  })
}

export function saveMenu(data) {
  return request({
    url: 'admin-service/api/roleMng/saveMenu',
    method: 'post',
    data
  })
}

export function saveOrg(data) {
  return request({
    url: 'admin-service/api/roleMng/saveOrg',
    method: 'post',
    data
  })
}

export function joinAdminUser(data) {
  return request({
    url: 'admin-service/api/roleMng/joinAdminUser',
    method: 'post',
    data
  })
}

export function findAllPrivList(data) {
  return request({
    url: 'admin-service/api/roleMng/findAllPrivList',
    method: 'post',
    data
  })
}
